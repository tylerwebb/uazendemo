; uazendemo make file for local development
core = "7.x"
api = "2"

projects[drupal][version] = "7.36"
; include the d.o. profile base
includes[] = "drupal-org.make"

; +++++ Features +++++

projects[ua_demo_content][type] = "module"
projects[ua_demo_content][subdir] = "custom"
projects[ua_demo_content][directory_name] = "ua_demo_content"
projects[ua_demo_content][download][type] = "git"
projects[ua_demo_content][download][url] = "git@bitbucket.org:mmunro-ltrr/ua_demo_content.git"

projects[ua_menu_subsystem][type] = "module"
projects[ua_menu_subsystem][subdir] = "custom"
projects[ua_menu_subsystem][directory_name] = "ua_menu_subsystem"
projects[ua_menu_subsystem][download][type] = "git"
projects[ua_menu_subsystem][download][url] = "git@bitbucket.org:mmunro-ltrr/ua_menu_subsystem.git"

projects[ua_content_types][type] = "module"
projects[ua_content_types][subdir] = "custom"
projects[ua_content_types][directory_name] = "ua_content_types"
projects[ua_content_types][download][type] = "git"
projects[ua_content_types][download][url] = "git@bitbucket.org:uabrandingdigitalassets/ua-features.git"

projects[ua_featured_content][type] = "module"
projects[ua_featured_content][subdir] = "custom"
projects[ua_featured_content][directory_name] = "ua_featured_content"
projects[ua_featured_content][download][type] = "git"
projects[ua_featured_content][download][url] = "git@bitbucket.org:uabrandingdigitalassets/ua_featured_content.git"

; +++++ Themes +++++

; UAZen
projects[ua_zen][type] = "theme"
projects[ua_zen][subdir] = "custom"
projects[ua_zen][directory_name] = "ua-zen"
projects[ua_zen][download][type] = "git"
projects[ua_zen][download][url] = "git@bitbucket.org:uabrandingdigitalassets/ua-zen.git"

; +++++ Libraries +++++

; ResponsiveMultiLevelMenu
libraries[ResponsiveMultiLevelMenu][directory_name] = "ResponsiveMultiLevelMenu"
libraries[ResponsiveMultiLevelMenu][type] = "library"
libraries[ResponsiveMultiLevelMenu][destination] = "libraries"
libraries[ResponsiveMultiLevelMenu][download][type] = "get"
libraries[ResponsiveMultiLevelMenu][download][url] = "https://github.com/codrops/ResponsiveMultiLevelMenu/archive/master.zip"

