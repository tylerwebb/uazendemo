#!/bin/bash
webparent=${1-'/usr/local/webdev'}
drupaldirname=${2-'uazendemoroot'}
distromakefile=${3-'distro_uazendemo.make'}
webgroup=${4-'www'}
profilename=${5-'uazendemo'}
rubyversion=${6-'2.2.1'}
rubygemset=${7-'uaweb.uazen'}
if [ -d "$webparent" ] && [ -x "$webparent" ]; then
  echo 'Writeable web parent directory...' >&2
else
  echo '** no writable web parent directory.' >&2
  exit 1
fi
drupalroot="$webparent/$drupaldirname"
if [ -d "$drupalroot" ]; then
  echo "Removing existing $drupalroot directory..." >&2
  sudo rm -Rf "$drupalroot"
  if [ -d "$drupalroot" ]; then
    echo "** unable to remove the existing $drupalroot directory..." >&2
    exit 1
  fi
fi
drush make "$distromakefile" "$drupalroot"
if [ -d "$drupalroot" ]; then
  echo "Made a new $drupalroot directory..." >&2
else
  echo "** unable to create a new $drupalroot directory." >&2
  exit 1
fi
echo "$rubyversion" > "$drupalroot/.ruby-version"
echo "$rubygemset" > "$drupalroot/.ruby-gemset"
if [ -r "$drupalroot/.ruby-version" ] && [ -r "$drupalroot/.ruby-gemset" ]; then
  echo "Can put readable files in the $drupalroot directory..."
else
  echo "** unable to put readable files in the $drupalroot directory." >&2
  rmdir "$drupalroot"
  exit 1
fi
sitesdefault="$drupalroot/sites/default"
profilesdir="$drupalroot/profiles"
for dir in "$sitesdefault" "$profilesdir"; do
  if [ -d "$dir" ] && [ -x "$dir" ]; then
    echo "Successfully created the $dir directory..." >&2
  else
    echo "** failed to install the $dir directory." >&2
    rm -Rf "$drupalroot"
    exit 1
  fi
done
settingsfile="$sitesdefault/settings.php"
cp "$sitesdefault/default.settings.php" "$settingsfile"
if [ -w "$settingsfile" ]; then
  echo "Made a new $settingsfile file..." >&2
else
  echo "** failed to copy $sitesdefault/default.settings.php to $settingsfile." >&2
  rm -Rf "$drupalroot"
  exit 1
fi
filesdir="$sitesdefault/files"
profile="$profilesdir/$profilename"
mkdir "$filesdir"
mkdir "$profile"
for subdir in "$filesdir" "$profile"; do
  if [ -d "$subdir" ] && [ -x "$subdir" ]; then
    echo "Successfully created the $subdir directory..." >&2
  else
    echo "** failed to make a $subdir directory." >&2
    rm -Rf "$drupalroot"
    exit 1
  fi
done
chgrp "$webgroup" "$settingsfile" "$filesdir"
chmod 2775 "$filesdir"
chmod 664 "$settingsfile"
for pfile in * ; do
  if [ -f "$pfile" ]; then
    cp $pfile $profile
    if [ -r "$profile/$pfile" ]; then
      echo "Copied $pfile to the profile..." >&2
    else
      echo "** failed to transcribe $profile/$pfile." >&2
      rm -Rf "$drupalroot"
      exit 1
    fi
  fi
done
exit 0

